package stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stram 转换为其他数据类型
 * @author PangHao
 * @date 2018/8/29  18:02
 */
public class TransformStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("a","b", "c");
        //转为数组
        String[] strArray1 = stream.toArray(String[]::new);
        // 转为 Collection  的实现类
        List<String> list1 = stream.collect(Collectors.toList());
        List<String> list2 = stream.collect(Collectors.toCollection(ArrayList::new));
        Set set1 = stream.collect(Collectors.toSet());
        //堆栈
        Stack stack1 = stream.collect(Collectors.toCollection(Stack::new));
        // 转为String
        String str = stream.collect(Collectors.joining()).toString();
    }
}
