package stream;

import java.util.*;
import java.util.stream.*;

/**
 * 构造流的几种常见方法
 * @author PangHao
 * @date 2018/8/29  16:54
 */
public class SimpleStream {
    public static void main(String[] args) {

        //01 通过Stream 自身的静态方法 of 构建
        Stream<Integer> intStream = Stream.of(1, 2, 3, 4, 5);


        //02 通过数组进行构建
        //本质通过Stream 自身的静态方法 of 构建
        String [] strArray = new String[] {"a", "b", "c"};
        Stream<String> strArray1 = Stream.of(strArray);

        //03 通过 Collections 的实现 构建
        List<String> list = Arrays.asList(strArray);
        Stream<String> stream = list.stream();



        //04 对于基本数值型，目前有三种对应的包装类型 Stream
        //常规的数值型聚合运算可以通过上面三种 Stream 进行。
        //和Stream<Integer>、Stream<Long> 、Stream<Double> 等效
        IntStream intStream1 = IntStream.of(1, 2, 3);
        DoubleStream doubleStream = DoubleStream.of(1.0, 2.0, 3.0);
        LongStream longStream = LongStream.of(1, 2, 3);

        //已IntStream为例
        //支持顺序和并行聚合操作的原始int值元素序列。 这是int原始专业Stream
         IntStream.of(new int[]{1, 2, 3}).forEach(System.out::println);
        //类似Python中的range 生成范围1-3的序列（包头不包尾）
        IntStream.range(1, 3).forEach(System.out::println);// 输出 1 2
        //包头也包尾的range
        IntStream.rangeClosed(1, 3).forEach(System.out::println);// 输出 1 2 3



    }
}
