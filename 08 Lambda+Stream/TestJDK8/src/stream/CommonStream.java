package stream;

import java.util.*;
import java.util.stream.*;

import static java.util.stream.IntStream.range;

/**
 * 常用Stream操作
 * @author PangHao
 * @date 2018/8/30  9:39
 */
public class CommonStream {
    public static void main(String[] args) {
        //Stream 操作大致能分为三种

        Stream<Integer> simpleStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        //Intermediate（媒介） 惰性求值方法（N）


        // map  返回由给定函数应用于此流的元素的结果组成的流
        // simpleStream.map(x->x+5+"\t").forEach(System.out::print);
        //返回由给定函数应用于此流的元素的结果组成的流
        //在map操作的基础上返回该数据类型对应的专业化Stream
        //IntStream intStream = simpleStream.mapToInt(x -> x + 5);
        //DoubleStream doubleStream = simpleStream.mapToDouble(x -> x + 5);
        //LongStream longStream = simpleStream.mapToLong(x -> x + 5);
        //小写元素转大写
        Stream<String> stringStream = Stream.of("a", "b", "c", "d");
        //stringStream.map(x->x.toUpperCase()).forEach(System.out::println);
        //可简写
        stringStream.map(String::toUpperCase).forEach(System.out::println);

        //filter 依据lambda过滤Stream元素
        long count = simpleStream.filter(x -> x > 4).count();
        System.out.println(count);

        //distinct 元素去重，对于有序流不影响其排序
        Stream<Integer> distinctStream = Stream.of(1, 2, 3, 4, 4, 4, 4, 5, 6, 7, 8, 9);
        distinctStream.distinct().map(x->x+"\t").forEach(System.out::print);
        System.out.println();

        //sorted 元素排序，按照自然循序或compare规则排序
        //自然顺序排序
        Stream<Integer> sortedStream = Stream.of(1, 5, 9, 7, 3, 6, 4, 8, 2,0);
        sortedStream.sorted().map(x->x+"\t").forEach(System.out::print);
        System.out.println();
        //倒序
        Stream<Integer> sortedStream1 = Stream.of(1, 5, 9, 7, 3, 6, 4, 8, 2,0);
        sortedStream1.sorted(Comparator.reverseOrder()).map(x->x+"\t").forEach(System.out::print);
        System.out.println();

        //返回由此流的元素组成的流，截短长度不要超过maxSize 。
        Stream<Integer> limitStream = Stream.of(1, 5, 9, 7, 3, 6, 4, 8, 2,0);
        limitStream.limit(3).map(x->x+"\t").forEach(System.out::print);
        System.out.println();

        //在丢弃流的第一个n元素后，返回由该流的剩余元素组成的流。 如果此流包含少于n元素，那么将返回一个空流。
        Stream<Integer> skip​Stream = Stream.of(1, 5, 9, 7, 3, 6, 4, 8, 2,0);
        skip​Stream.skip(3).map(x->x+"\t").forEach(System.out::print);


        //toArray
        Stream<Integer> arrayStream = Stream.of(1, 5, 9, 7, 3, 6, 4, 8, 2,0);
        //无参，返回一个Object数组
        Object[] objects = arrayStream.toArray();
        //发生器函数采用一个整数，它是所需数组的大小，并产生一个所需大小的数组
        //构造引用 ，限定返回数组类型
        //String[] strings = arrayStream.toArray(String[]::new);
        System.out.println();

        //reduce 减少操作
        //T reduce​(T identity,BinaryOperator<T> accumulator)
        // 使用提供的身份值和associative累积功能对此流的元素执行reduction ，并返回减小的值
        IntStream range = IntStream.rangeClosed(1, 10);
        int reduce = range.reduce(0, Integer::sum);
        System.out.println(reduce);
        //等价于
        IntStream range1 = IntStream.rangeClosed(1, 10);
        int reduce1 = range1.reduce(0, (a, b) -> a + b);
        System.out.println(reduce1);
        //Optional<T> reduce​(BinaryOperator<T> accumulator)
        IntStream range2 = IntStream.rangeClosed(1, 10);
        OptionalInt reduce2= range2.reduce((a, b) -> a + b);
        int asInt = reduce2.getAsInt();
        System.out.println(asInt);


        //collect 将Stream转换为其他数据结构
        //提供了两个重载的方法

        //<R> R collect​(Supplier<R> supplier,  BiConsumer<R,? super T> accumulator, BiConsumer<R,R> combiner)
        //supplier：一个能创造目标类型实例的方法。
        // accumulator：一个将当元素添加到目标中的方法。
        // combiner：一个将中间状态的多个结果整合到一起的方法（并发的时候会用到）
        IntStream stream = Arrays.stream(range(1, 10).toArray());
        ArrayList<Object> collect = stream.collect(ArrayList::new, List::add, List::addAll);
        System.out.println(collect.toString());

        //<R,A> R collect​(Collector<? super T,A,R> collector)
        //注：Stream的特化IntStream，LongStream，DoubleStream不具备该方法
        List<String> strings = Arrays.asList("a", "b", "c", "d", "e");
        //可以通过Collectors提供的静态方法返回各种数据，其实就是另一个方法的封装简化
        //List->Set
        Set<String> collect1 = strings.stream().collect(Collectors.toSet());
        System.out.println(collect1.toString());
        //List->Map
        Map<String, String> collect3 = strings.stream().collect(Collectors.toMap(x -> x.toUpperCase(), y -> y.toString()));
        System.out.println(collect3.toString());


        IntStream stream1 = Arrays.stream(range(1, 10).toArray());
        //anyMatch   表示，判断的条件里，任意一个元素成功，返回true
        System.out.println(stream1.anyMatch(x->x>5)); //返回true

        //allMatch   表示，判断条件里的元素，所有的都是，返回true
        //System.out.println(stream1.allMatch(x->x>0)); //返回true

        //noneMatch  跟allMatch相反，判断条件里的元素，所有的都不是，返回true
        //System.out.println(stream1.noneMatch(x->x<0)); //返回true



    }
}
