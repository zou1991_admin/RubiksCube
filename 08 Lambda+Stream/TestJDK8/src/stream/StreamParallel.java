package stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;

/**
 * Stream 对并发的优化
 * @author PangHao
 * @date 2018/8/31  14:59
 */
public class StreamParallel {
    public static void main(String[] args) {

        //JAVA 8
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6);
        Stream<Integer> integerStream = integers.parallelStream();


    }


}
