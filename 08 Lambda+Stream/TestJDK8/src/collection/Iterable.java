package collection;

import java.util.*;
import java.util.concurrent.CountedCompleter;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.IntStream.range;

/**
 * Iterable 迭代
 * Collection 实现了该接口，其内部提供了迭代的相关方法
 * @author PangHao
 * @date 2018/8/31  9:54
 */
public class Iterable {
    public static void main(String[] args) {

        //Iterator<T> iterator​()
        //返回一个类型为 T元素的迭代器。
        /*List<String> collect = Stream.of("a", "b", "c").collect(Collectors.toList());
        Iterator<String> iterator = collect.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }*/


        // default void forEach​(Consumer<? super T> action)
        //since 1.8
        //对Iterable每个元素执行给定的操作，直到所有元素都被处理或者动作引发异常。 如果指定了该顺序，则按迭代的顺序执行操作。
        // 动作抛出的异常被转发给呼叫者。
        //如果操作执行修改元素的基础源的副作用，则该方法的行为是未指定的，除非重写类已指定并发修改策略。

        //collect.forEach(System.out::println);
        //等价于
        //collect.forEach(x->System.out.println(x));


        ArrayList<Object> collect1 = IntStream.rangeClosed(1, 10).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        //01 容器调用spliterator并返回一个Spliterator
        Spliterator<Object> spliterator = collect1.spliterator();

        //将这个Spliterator分割三个Spliterator出来
        Spliterator<Object> objectSpliterator = spliterator.trySplit();
        Spliterator<Object> objectSpliterator1 = spliterator.trySplit();
        Spliterator<Object> objectSpliterator2 = spliterator.trySplit();

        //四个Spliterator 并行去迭代容器
        //spliterator.forEachRemaining(System.out::println);
        spliterator.forEachRemaining(x-> System.out.println("并行迭代father:"+x));
        objectSpliterator.forEachRemaining(x-> System.out.println("并行迭代1:"+x));
        objectSpliterator1.forEachRemaining(x-> System.out.println("并行迭代2:"+x));
        objectSpliterator2.forEachRemaining(x-> System.out.println("并行迭代3:"+x));

        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
        integerStream.spliterator().forEachRemaining(System.out::println);


    }
}
