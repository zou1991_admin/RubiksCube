package simpleLambda;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * lambda基础函数式接口
 * Java8 内置的四大核心函数式接口
 * java提供的函数式接口放置在java.util.function 下
 * @author PangHao
 * @date 2018/8/24  11:16
 */
public class Test03 {
    public static void main(String[] args) {
        /**
         * 01 Interface Consumer<T> 消费型接口 (一个入参，无返回值)
         * 表示接受单个输入参数并且不返回结果的操作。 与大多数其他功能接口不同， Consumer预期通过副作用进行操作。
         * 这是一个functional interface的功能方法是accept(Object) 。
         */
        Consumer consumer =x-> System.out.println("hello "+x);
        //控制台输出 “hello PangHao”
        consumer.accept("PangHao");


        /**
         * 02 Interface Supplier<T> 供给型接口
         * 代表结果供应商。
         * 没有要求每次调用供应商时都会返回新的或不同的结果。
         * 这是一个functional interface的功能方法是get() 。
         */
        Supplier supplier=()->"返回一个object对象";
        Object o = supplier.get();
        System.out.println(o);

        /**
         * 03 Interface Function<T,R> 函数型接口
         * T - 函数输入的类型
         * R - 函数结果的类型
         * 表示接受一个参数并产生结果的函数。
         * 这是一个functional interface的功能方法是apply(Object)
         */
        Function function= x-> x+"加个字符串";
        Object apply = function.apply(3);
        System.out.println(apply);

        /**
         * 04  Interface Predicate<T> 断定型接口
         * 表示一个参数的谓词（布尔值函数）。
         * 这是一个functional interface，其功能方法是test(Object)
         * 确定类型为T的对象是否满足某约束，并返回boolean 值
         */
        Predicate<Integer> predicate=(x)->x>5;
        boolean test = predicate.test(9);
        System.out.println(test);
    }
}
