package simpleLambda;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.function.IntBinaryOperator;

/**
 * lambda 表达式基本语法
 * @author PangHao
 * @date 2018/8/24  10:06
 */
public class Test01 {
    public static void main(String[] args) {
        // 01 无参数，无返回值
        // () -> System.out.println("hello World")
        Runnable hello_world = () -> System.out.println("hello World");
        hello_world.run();

        // 02 一个参数，无返回值
        //(x) -> System.out.println("hello  "+x)
        //如果只有一个参数,括号可省略，如： x -> System.out.println("hello  "+x)
        Consumer hello_world1 = (x) -> System.out.println("hello  "+x);
        hello_world1.accept("PangHao");

        //03 多个参数有返回值，附带代码块
        //(x,y)->x+y
        IntBinaryOperator sumInt = (x, y) -> {
            System.out.println(x + "+" + y + "=" + (x + y));
            return x + y;
        };
        System.out.println(sumInt.applyAsInt(3,5));

        //如果不使用代码块，return 可省略
        IntBinaryOperator sumInt1 = (x, y) -> x + y;
        System.out.println("sumInt1="+sumInt1.applyAsInt(3,4));

        //如果单行语句不用写return和大括号
        IntBinaryOperator tComparator = (x, y) -> x + y;
        System.out.println(tComparator.applyAsInt(7,8));

        /*
         *  Lambda 表达式中的参数类型都是由编译器推断得出的。
         *  Lambda 表达式中无需指定类型，程序依然可以编译，这是因为 javac 根据程序的上下文，
         *  在后台推断出了参数的类型。 Lambda 表达式的类型依赖于上下文环境，是由编译器推断出来的。这就是所谓的 “类型推断”
         */
        // 04 Lambda 表达式的参数列表的数据类型可以省略不写，因为JVM编译器通过上下文推断出，数据类型，即“类型推断”
        Comparator<Integer> integerComparator01 = ( Integer x,  Integer y) -> Integer.compare(x, y);
        System.out.println( integerComparator01.compare(6,5));
        //还可以简化
        Comparator<Integer> integerComparator02 = ( x,  y) -> Integer.compare(x, y);
        System.out.println( integerComparator02.compare(3,5));

    }
}

