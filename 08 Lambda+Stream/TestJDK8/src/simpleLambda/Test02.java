package simpleLambda;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * lambda表达式基本原理 01
 * @author PangHao
 * @date 2018/8/24  10:34
 */
public class Test02 {

    //01 lambda的使用需要 “函数式接口” 的支持
    //常用函数式接口方法 java.util.function 接口通常用@FunctionalInterface标识(非必须)
    /*函数式接口：接口中只有一个抽象方法的接口，称为函数式接口，
    可以通过 Lambda 表达式来创建该接口的对象 (若 Lambda表达式抛出一个受检异常，那么该异常需要在目标接口的抽象方法上进行声明)
    可以使用注解 @FunctionalInterface 修饰可以检查是否是函数式接口，同时 javadoc 也会包含一条声明，说明这个接口是一个函数式接口
     */
    //测试自定义函数式接口
    public static void main(String[] args) {

        TestInterface testInterface =(x)-> "Hellow "+x;
        //控制台输出  Hellow PangHao
        System.out.println(testInterface.testFunction("PangHao"));


        //以Lambda表达式作为参数传递，简化遍历过滤
        List<Integer> list = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11).collect(Collectors.toList());

        //使用双冒号可以直接在控制台循环输出每个元素
        list.stream().filter(str->str>5).forEach(System.out::println);

        long count = list.stream().filter(str -> str > 5).count();
        System.out.println("The Stream Count Is "+count);

        //将Lambda 表达式作为方法参数传到方法中
        //传入一个Integer值 x，如果值 x>5 返回true,否则返回false
        int filters = filters(list, x -> x > 5 ? true : false);
        // 输出 “The Count Is 6”
        System.out.println("The Function Count Is "+filters);

    }

    //实际对list进行过滤的方法，可以传递不同的判定规则
    //判断该list 中满足判定规则元素的个数
    public static int filters(List<Integer> list, SimpleFilter<Integer>  fil) {
        //SimpleFilter 是自定义的一个函数式接口
        int count = 0;
        for (int no : list) {
            //这里去调用SimpleFilter 的功能性方法并将值传入
            if (fil.judge(no)) {
                count++;
            }
        }

        //也可以用stream 来简化这种操作（推荐）
        long count1 = list.stream().filter((s) -> s > 5).count();
        System.out.println("count1="+count1);
        //输出true
        System.out.println(count1==count);
        return count;
    }







}

