package simpleLambda;

import java.util.function.Consumer;

/**
 * lambda 数组
 * @author PangHao
 * @date 2018/9/3  9:20
 */
public class lambdaCollection {
    public static void main(String[] args) {
        //lambda数组初始化与构造器初始化类似，只不过其类型为其组件类型
        Consumer [] consumers =new Consumer[]{
                x-> System.out.println("hello x"),
                y-> System.out.println("hello y"),
                z-> System.out.println("hello z"),
        };

        consumers[0].accept(0);
        consumers[1].accept(1);
        consumers[2].accept(2);
    }
}
