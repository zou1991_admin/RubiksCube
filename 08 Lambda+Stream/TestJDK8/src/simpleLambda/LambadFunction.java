package simpleLambda;

import javafx.beans.binding.IntegerBinding;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Lambda函数接口
 * java.util.function下
 * @author PangHao
 * @date 2018/8/23  16:21
 */
public class LambadFunction {
    public static void main(String[] args) {
       /* Function<Integer,Integer> test=(x)->x+1;
        System.out.println(test.apply(3));*/

        List<String> strings = Arrays.asList(new String[]{"a", "b", "c"});

        long count = strings.stream().filter(string -> "a".equals(string)).count();
        System.out.println(count);
        strings.stream().filter(string->{
            System.out.println(123);
            return "a".equals(string);
        }).count();
        strings.stream().forEach(string-> System.out.println(string));

    }



}

