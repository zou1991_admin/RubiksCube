package simpleLambda;

/**
 * @author PangHao
 * @date 2018/8/24  18:17
 */
@FunctionalInterface
public interface SimpleFilter <Integer>{

    /**
     * 功能性方法，传入一个Integer 返回一个 boolean
     * @param t 一个Integer 值
     * @return: boolean
     * @auther: PangHao
     * @date:  2018/8/24  18:19
     */
    boolean judge(Integer t);
}
