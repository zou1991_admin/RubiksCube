package simpleLambda;

import java.util.function.IntUnaryOperator;

/**
 * lambda的递归
 * @author PangHao
 * @date 2018/8/31  15:48
 */
public class LambdaRecursion {

    //IntUnaryOperator 表示对单个int值的操作数的操作，产生一个int值结果。 这是int的int的原始类型专业化。
    static IntUnaryOperator  fact;

    public static void main(String[] args) {
        //如果声明的名字的在作用域中，那么lambda就可以引用自身
        //如实现递归,注意必须先声明
        fact=x->{
            System.out.print(x+"\t");
            return  x>0?x:fact.applyAsInt(x+1) ;
        };
        fact.applyAsInt(-4);
    }
}
