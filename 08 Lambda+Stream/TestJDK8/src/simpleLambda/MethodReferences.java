package simpleLambda;

import java.io.FilenameFilter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.IntStream.range;

/**
 * java 方法引用
 * 1.8 引入的符号
 * @author PangHao
 * @date 2018/8/24  15:58
 */
public class MethodReferences {

    public static void main(String[] args) {


/*

        */
/**
         * 01 对象的引用 :: 实例方法名
         *//*

        PrintStream out = System.out;
        Consumer consumer=out::println ;
        consumer.accept("PnagHao");

        //JDK8中，接口Iterable 8中默认实现了forEach方法，调用了 JDK8中增加的接口Consumer内的accept方法，执行传入的方法参数。
        List<String> al = Arrays.asList("a", "b", "c", "d");
        al.forEach(out::println);
        //下面的方法和上面等价的
        Consumer<String> methodParam = out::println; //方法参数
        al.forEach(x -> methodParam.accept(x));//方法执行accept

        BiFunction<Integer,Integer,Integer> biFunction=(a, b) -> Math.max(a, b);
        BiFunction<Integer,Integer,Integer> biFunction1=Math::max;
        System.out.println(biFunction.apply(3,5)==biFunction1.apply(3,5));

        Consumer<String> tConsumer = x -> System.out.println(x);
        Consumer<String> tConsumer1 = System.out::println;
        tConsumer1.accept("PnagHao");
        tConsumer.accept("PnagHao");


        //构造器的引用
        Supplier supplier=ArrayList::new;
        ArrayList list = (ArrayList)supplier.get();
        System.out.println(list); //輸出[] 而非null


        //绑定引用
        //接收者已经确定为方法引用的一部分（每次调用都会有相同的接收者）
        ArrayList<Object> collect = range(1, 10).collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
       // collect.forEach(System.out::println);
*/

        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5, 6, 7);
        List<Integer> collect1 = integerStream.collect(Collectors.toList());

        IntStream range = range(1, 8);

        ArrayList<Object> collect = range.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

        System.out.println(collect.toString());
        System.out.println(range);
        range.map(x->x+1);
        
    }
}
