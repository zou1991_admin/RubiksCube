package simpleLambda;

import java.util.function.*;

/**
 * 常用Lamdba函数式接口
 * @author PangHao
 * @date 2018/8/24  14:56
 */
public class Test04 {
    public static void main(String[] args) {

        /**
         * Interface ToIntFunction<T>
         * 计算 int 值的函数可以用来拆包
         * ToIntFunction<T>
         * ToLongFunction<T>
         * ToDoubleFunction<T> 这三个方法作用类似，不再赘述
         */
        ToIntFunction<Integer> toIntFunction=x->x;
        int i = toIntFunction.applyAsInt(new Integer(5));

        ToIntFunction toIntFunction1=x-> {
            System.out.println("放个代码块看看 "+x);
            return 5;
        };
        System.out.println(toIntFunction1.applyAsInt("PangHao"));

        /**
         * Interface IntFunction<R>
         * 表示一个接受int值参数并产生结果的函数。 这是int消耗原始专业Function 。
         * 这是一个functional interface的功能方法是apply(int) 。
         * IntFunction<R>
         * LongFunction<R>
         * DoubleFunction<R> 这三个方法作用类似，不再赘述
         */
        IntFunction intFunction =x->"传进来了个int值："+x;
        Object apply2 = intFunction.apply(5);
        System.out.println(apply2);

        /**
         * Interface BiFunction<T,U,R>
         * T - 函数的第一个参数的类型
         * U - 函数的第二个参数的类型
         * R - 函数结果的类型
         * 表示接受两个参数并产生结果的函数。 这是二位一体的专业化Function 。
         * 这是一个functional interface的功能方法是apply(Object, Object) 。
         */
        BiFunction<Integer,Integer,String> biFunction =(x,y)->x+"+"+y+"="+(x+y);
        String apply = biFunction.apply(3, 5);
        System.out.println(apply);

        /**
         * Interface UnaryOperator<T>
         * T - 操作数的类型和运算符的结果
         * 对类型为T的对象进行一元运算， 并返回T类型的结果表示对单个操作数产生与其操作数相同类型的结果的操作。
         * 对于操作数和结果是相同类型的情况，这是Function的专业化。
         * 这是一个functional interface，其功能方法是Function.apply(Object) 。
         *
         */
        UnaryOperator<Integer> unaryOperator=x->x+5;
        Integer apply1 = unaryOperator.apply(5);
        System.out.println(apply1);

        /**
         * Interface BiConsumer<T,U>
         * T - 操作的第一个参数的类型
         * U - 操作的第二个参数的类型
         * 表示接受两个输入参数并且不返回结果的操作。 这是Consumer的二元专业化。 与大多数其他功能接口不同， BiConsumer预期通过副作用进行操作。
         * 这是一个functional interface的功能方法是accept(Object, Object)
         */
        BiConsumer biConsumer=(x,y)-> {
            System.out.println("参数1是："+x);
            System.out.println("参数2是："+y);
        };
        biConsumer.accept("PangHao","ZhangSan");

        /**
         * Interface BinaryOperator<T>
         * T - 操作数的类型和操作符的结果
         * 表示对同一类型的两个操作数的操作，产生与操作数相同类型的结果。 对于操作数和结果都是相同类型的情况，
         * 这是一个BiFunction的专业化。
         * 这是一个functional interface的功能方法是BiFunction.apply(Object, Object) 。
         */
        BinaryOperator<Integer> binaryOperator =(x,y)->x+y;
        Integer apply3 = binaryOperator.apply(3, 5);
        System.out.println(apply3);


    }
}
