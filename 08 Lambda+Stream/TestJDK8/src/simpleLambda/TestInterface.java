package simpleLambda;

/**
 * 自定义函数式接口
 * @param: <T> the type of results supplied by this supplier
 * @author PangHao
 * @date 2018/8/29  18:38
 */
@FunctionalInterface
public interface TestInterface<T> {

    /**
     * 自定义函数式接口的功能方法，返回和入参类型相同的一个值
     * @param: <T> the type of results supplied by this supplier
     * @return: <T> he type of results supplied by this supplier
     * @auther: PangHao
     * @date:  2018-08-29 10:05
     */
    T testFunction(T t);

}
