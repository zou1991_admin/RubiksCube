package LambdaByRunnable;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.*;

/**
 * 以Lambda表达式作为参数传递，简化匿名内部类操作
 * @author PangHao
 * @date 2018/8/24  10:56
 */
public class Test {
    public static void main(String[] args) {
        //传统创建线程方法
        TestRunnableImpl testRunnable = new TestRunnableImpl();
        Thread thread = new Thread(testRunnable);
        thread.run();

        //匿名内内部类防水
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("匿名内部类式");
            }
        });
        thread1.run();

        //lambda 式
        Runnable run=()-> System.out.println("使用Lambda创建线程");
        Thread thread2 = new Thread(run);
        thread2.run();

        //二次简化
        new Thread(()-> System.out.println("简化Lambda式")).run();


        //线程池+Lambda+Google Guava
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("PangHao-test-Thread-%d").build();
        ExecutorService singleThreadPool = new ThreadPoolExecutor(2, 2,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());

        //控制台输出 “使用Guava线程池来创建线程,线程名称为：PangHao-test-Thread-0”
        singleThreadPool.execute(()-> System.out.println("使用Guava线程池来创建线程,线程名称为："+Thread.currentThread().getName()));
        //控制台输出 “使用Guava线程池来创建线程,线程名称为：PangHao-test-Thread-1”
        singleThreadPool.execute(()-> System.out.println("使用Guava线程池来创建线程,线程名称为："+Thread.currentThread().getName()));
        singleThreadPool.shutdown();
    }

}
