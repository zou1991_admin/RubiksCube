#dict全称dictionary，等同java中的map.使用key-value存储数据
#dict 的 key 必须为不可变对象
dictMap={1:'a',2:'b',3:'c'}
print("一个简单的dict: dictMap=",dictMap)
print("通过dict[key] 的形式可以得到对应的value")
print("输出 dictMap[1] 得到:",dictMap[1])
print("通过dict[key]=value 的形式可以修改key对应的value")
print("修改前：",dictMap[1])
dictMap[1]='A'
print("修改后：",dictMap[1])


print("通过in判断key是否存在")
print("打印  'A' in dictMap")
print('A' in dictMap)
print(1 in dictMap)

print("通过.gat(key)判断key是否存在,如不存在返回None")
print("打印 dictMap.get('A')")
print(dictMap.get('A'))
print(dictMap.get(1))
print("通过.gat(key,default=None)判断key是否存在,如不存在返回第二个参数")
print("打印 dictMap.get('A','该key不存在')")
print(dictMap.get('A','该key不存在'))

print("通过pop(key)移除dict内元素")
dictMap.pop(1)
print(dictMap)