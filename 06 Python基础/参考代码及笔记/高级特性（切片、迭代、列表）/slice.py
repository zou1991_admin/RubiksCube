#切片（Slice）操作符   包前不包后
list1=["ZouHuaQin","RenJiaHang","PangHao","XiaShuCheng","HuMuQiang","GuanZhao"]

 
print(list1)
#从索引0元素开始取，直到索引3（不包括3）为止
print(list1[0:3])
#如果开始索引为0，可以将0省略
print(list1[:3])
#从索引1元素开始取，直到索引3（不包括4）为止
print(list1[1:4])
#也可以从后往前取，倒数第一个元素为-1,注意从小到大的顺序写（从左往右）
print(list1[-3:-1])
#0到倒数第二个
print(list1[:-2])
#最后两个
print(list1[-2:])
#前6个元素，每两个取一个
print(list1[:6:2])
#所有元素，每两个取一个
print(list1[::2])
#取所有元素（复制list）
print(list1[:])

#tuple也可以用切片操作,但tuple不可变

#str也可以切片
print("让我们将夏术程同学（名字比较长）切片一下：","XiaShuCheng"[1:-1],'掐头去尾')
