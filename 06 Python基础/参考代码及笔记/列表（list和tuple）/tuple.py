#另一种有序列表叫元组：tuple。tuple和list非常类似，但是tuple一旦初始化就不能修改
#tuple 的不可更改指的是引用，如果引用对象的内容发生变化，tuple的实际内容也会改变
#声明：tuple1=(1,2,3,4,5)   tuple()
tuple1=(1,2,3,4,5)
print("这是一个简单的tuple元组：",tuple1)

tuple2=('A','B','C',['D','E','F'])
print("这是一个新的tuple元组，里面包含了一个list：",tuple2)
tuple2[3][0]='X'
tuple2[3][1]='Y'
tuple2[3][2]='Z'
print("这是更改了list内容的tuple元组：",tuple2)
print("\n")
print("tuple所谓的“不变”是说，tuple的每个元素，指向永远不变。即指向'a'，就不能改成指向'b'，\n指向一个list，就不能改成指向其他对象，但指向的这个list本身是可变的！")