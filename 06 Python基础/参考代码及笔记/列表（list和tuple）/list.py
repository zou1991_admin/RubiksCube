#list是一种"可变"的有序的集合，可以随时添加和删除其中的元素。
#可以通过len(list) 方法得到集合长度，最后一个元素的索引是len(classmates) - 1
classmates = ['Michael', 'Bob', 'Tracy']
print(classmates)

#可以通过角标去取内部元素
iList=list(range(len(classmates)))
#print(iList)
print("通过角标依次得到list内元素")
for i in iList:
	print(classmates[i])


#可用list[-1] 来获取list的最后一个元素 ，以此类推
print("通过角标list[-i]依次从最后往前得到元素")
for i in iList:
	k=i+1
	print(classmates[-k])





#list是一个可变的有序表,所以可以对其内部元素进行增删改

print("通过.append(item)方法对list末尾追加元素")
newItem=input("请输入新的元素：")
classmates.append(newItem)
print(classmates)


print("=================================================")
print("通过.insert(indes,item) 方法对指定位置添加元素")
index1=input("请输入要插入元素的位置，从0开始：")
index1=int(index1)
newItem=input("请输入新的元素：")
classmates.insert(index1,newItem)
print(classmates) 


print("=================================================")
print("通过.pop(indes) 删除指定位置元素")
index2 =input("请输入要删除元素的位置，从0开始,不输入为删除末尾元素：")
if index2=='':
    classmates.pop()
else :
    classmates.pop(int(index2))
print(classmates)

print("=================================================")
print("通过角标list[indes] 修改指定位置元素")
index3 =input("请输入要修改元素的位置，从0开始：")
cheItem=input("请输入修改的元素：")
classmates[int(index3)]=cheItem
print(classmates)

