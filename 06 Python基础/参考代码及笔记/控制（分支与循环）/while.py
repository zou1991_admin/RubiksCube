#简单while循环
#continue语句会直接继续下一轮循环，后续的print()语句不会执行
#break 跳出循环
i=10
while i>0:
	if i==5:
		break
	print(i)
	i-=1
print("end")