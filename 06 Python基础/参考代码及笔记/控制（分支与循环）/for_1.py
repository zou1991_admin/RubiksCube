#普通循环
#计算1-100的整数之和
# range() 函数返回的是一个可迭代对象（类型是对象），而不是列表类型， 所以打印的时候不会打印列表。
# list()函数是对象迭代器，可以把range()返回的可迭代对象转为一个列表，返回的变量类型为列表。
name =range(10)
print("运行range(10)后的结果为：",name)
name=list(name)
print("运行list(name)后的结果为：",name)
sum=0
for num1 in name :
	print(num1)
	sum=sum+num1
print("sum=",sum)