#设置默认参数
#为多入参方法设置默认值,如果调用函数时参数个数与函数要求参数个数不符就会报错
#使用参数默认值的方法可以避免缺少参数个数报错的问题，且不影响传入正常个数参数方法的使用
def testFunction1(num1,num2=1):
	return num1+num2


t=testFunction1(5)
print(t)
t=testFunction1(5,5)
print(t)




#定义默认参数要牢记一点：默认参数必须指向不变对象！否则会累积
#错误例子如下
def testFunction2(L=[]):
	L.append('END！')
	return L

listn =list(range(10))
for i in listn:
    testFunction2()
print(testFunction2())


#可变参数
#入的参数个数是可变的，可以是1个、2个到任意个，还可以是0个
#在参数前面加了一个*号。表示在函数内部，参数numbers接收到的是一个tuple
def testFunction3(*par):
	sum=0
	for i in par:
		sum=sum+i
	return sum

k=testFunction3(1,2,3,4,5,6,7,8,9)
print(k)
 

#关键词参数
#关键字参数允许你传入0个或任意个含参数名的参数，这些关键字参数在函数内部自动组装为一个dict

def testFunction4(**dict1):
	print("输入的关键词参数：",dict1)
	return dict1

i=testFunction4(city='BeiJing',name='PangHao')
print("函数返回的结果：",i)



#命名关键字参数
#关键字参数需要一个特殊分隔符*，*后面的参数被视为命名关键字参数。
def testFunction5(*,name,age):
	print(name,age)
	return name,age
i=testFunction5( name='PangHao',age=18 )
print(i)


#参数组合
#在Python中定义函数，可以用必选参数、默认参数、可变参数、关键字参数和命名关键字参数，这5种参数都可以组合使用。 
#参数定义的顺序必须是：必选参数、默认参数、可变参数、命名关键字参数和关键字参数。


#如果函数定义中已经有了一个可变参数，后面跟着的命名关键字参数就不再需要一个特殊分隔符*了：