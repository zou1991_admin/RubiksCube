#声明一个简单的函数
def testFunction(num):
	print("方法入参:",num)
	return num+999



i=input("请输入一个数字");
k=testFunction(int(i))
print("返回的内容：",k)

#一个函数可以返回多个参数
def restFunction01():
	return 1,2,3
#函数返回的多个值可以用多个变量接受
a,b,c=restFunction01()
print(a,b,c)
#用一个值接受也行，他会存成一个tuple返回回来
d=restFunction01()
print(d)